# Trilinear-augmented gaugino mediation

This is the code used to produce the results in _Trilinear-augmented gaugino
mediation_, published in 
[JHEP05(2017)003](https://doi.org/10.1007/JHEP05(2017)003), available also on 
the [arXiv](arxiv.org/abs/1701.02313).

## Dependencies
Parameter scan

 - SPheno (spheno.hepforge.org)
 
 - FeynHiggs (wwwth.mpp.mpg.de/members/heinemey/feynhiggs/cFeynHiggs)

Collider check

 ver_2018: (_recommended_)
 - CheckMATE version 2 (checkmate.hepforge.org)
 
 ver_0: (_possible_)
 - CheckMATE version 1 (checkmate.hepforge.org)
 - MadGraph (madgraph.physics.illinois.edu)
        _NB! MadGraph needs to be installed with Pythia._

The installation paths must be set in env_scan.sh and/or env_collider.sh

The program versions used to produce the results in the paper are
- SPheno-3.3.8
- FeynHiggs 2.11.3, 2.10.0, 2.12.2
- MG5_aMC_v2_3_3
- CheckMATE-1.2.2
... but using the most recent versions is of course better.

## Structure
**Scan** contains all the files necessary for running the grid scan.

**Collider** contains the code and prescription for doing the collider checks.

  - - - -

New HEPP-ers can of course feel free to use this code as an introduction to

grid scanning supersymmetric parameter spaces, and any questions concerning the

code can be directed to inga (at) strumke (dot) com.


The scan can easily be extended to use other spectrum generators (such as
SoftSUSY, SuSpect, etc).