Before the running the grid scan, please open env_scan.py and
provide the path to your SPheno and FeynHiggs installations.

No further actions should be required to run the scan, so you're 
ready to launch

$ python grid_scan.py

For running test with fewer points and no Higgs mass restriction, do

$ python grid_scan.py --test

However, if you wish, you can also change settings in env_scan.py, such as
whether to keep SLHA input or FeynHiggs output files during the scani, or change
the accepted Higgs mass window (the default is the range 123-127 GeV).

If you keep the default settings, a directory called slha_files_from_scan is
created, and the SPheno output files from the scan are stored here.  
A file scan_results.dat is also written, containing a summary of the input  -
i.e.  the common scalar mass (m0), the common gaugino mass (m12), the common
trilinear coupling (A0), tan beta, the soft Higgs masses squared (m2H1, m2H2) -
and output parameters - being the lighest Higgs mass, the PDG code identity of
the LSP and the LSP mass.

# === TESTS ===

For testing that your pyslha and SPheno installations work, you can run

$ python scan_helper_functions.py --test

For performing a test of the grid scan over a few points, run

$ python grid_scan.py --test
