# --- Inga Strumke

"""
This module contains the Standard Model parameters which are written to the
SMINPUTS block in the SLHA file created. The parameter values should be updated
as new PDG data becomes available.
"""
import pyslha

# =================================================================================
# === Please make sure these values are up to date
ALPHA_EM = 127.918          # Elecgtromagnetic coupling at m_Z
G_F = 1.166379e-05          # Fermi-constant
ALPHA_S = 0.1181            # Strong coupling at m_Z
M_Z = 91.1876               # Z-mass at pole
M_B = 4.18                  # b-mass at b-mass
M_TOP = 173.2               # top-mass at pole
M_TAU = 1.77686             # tau mass at pole

# =================================================================================

def main():
    """
    Returns pyslha block containing the Standard Model input parameters
    Modify values here as they are updated by experiments.
    """
    sminputs = pyslha.Block("SMINPUTS")

    # --- In pyslha Blocks, keys are indices
    sminputs_keys = [1, 2, 3, 4, 5, 6, 7]

    # --- The entries in SMINPUTS are
    # --- alpha_em(m_Z), G_F, alpha_s(M_Z), m_Z(pole), m_b(mb), m_top(pole), m_tau(pole)
    sminputs_entries = [ALPHA_EM, G_F, ALPHA_S, M_Z, M_B, M_TOP, M_TAU]

    for key, entry in zip(sminputs_keys, sminputs_entries):
        sminputs[key] = entry

    return sminputs
