# --- Inga Strumke

"""
Helper functions for the grid scan, all based on the SLHA file conventions
as described in arxiv.org/abs/hep-ph/0311123
"""

import subprocess
import os
import pyslha

# --- These are the SLHA conventions for MINPAR and EXTPAR entries

MINPAR_INDICES = {"m0" : 1, "m12" : 2, "tanb" : 3, "signmu" : 4, "A0" : 5}
MINPAR_KEYS = MINPAR_INDICES.keys()

EXTPAR_INDICES = {"M1" : 1, "M2" : 2, "M3" : 3,
                  "At" : 11, "Ab" : 12, "Atau" : 13,
                  "m2H1" : 21, "mHd" : 21, "m2H2" : 22, "mHu" : 22,
                  "mu" : 23, "mA2" : 24, "tanb" : 25, "mA0" : 26,
                  "mH+" : 27, "mH" : 27, "m_eL" : 31, "m_muL" : 32,
                  "m_tauL" : 33, "m_eR" : 34, "m_muR" : 35, "m_tauR" : 36,
                  "m_q1L" : 41, "m_q2L" : 42, "m_q3L" : 43, "m_uR" : 44,
                  "m_cR" : 45, "m_tR" : 46, "m_dR" : 47, "m_sR" : 48,
                  "m_bR" : 49,
                 }
EXTPAR_KEYS = EXTPAR_INDICES.keys()

def make_slha_file(sminputs, values_dict, slha_file_name="slha_input", modsel_value=1,
                   generator_input=None):
    """
    Creates slha file from the provided sminputs (pyslha block), the
    values_dict (dictionary), the slha file name (default slha_input), the
    modsel value (default 1), and the optional generator input (pyslha block).
    Note that any pre-existing file with the slha_file_name will be
    overwritten.
    """

    # --- Ensure input types
    if not all((isinstance(sminputs, pyslha.Block),
                isinstance(values_dict, dict))):
        raise TypeError("SMINPUTS must be type pyslha.Block and the input "
                        "values must be in a dictionary.")

    # --- Pyslha dictionaries are just OrderedDicts (from Collections),
    # --- so you can use that if it makes you more comfortable
    all_blocks = pyslha._dict()

    # -- Add the sminputs to the pyslha dictionary containing all the blocks
    all_blocks["SMINPUTS"] = sminputs

    # --- Make the MODSEL block from the modsel_value, which must be a number
    assert isinstance(modsel_value*1.0, float), "The MODSEL value must be a number."

    modsel = pyslha.Block("MODSEL")
    modsel[1] = modsel_value

    # --- and add the block to the pyslha dictionary
    all_blocks["MODSEL"] = modsel

    # --- Make the MINPAR and EXTPAR blocks and fill them
    minpar = pyslha.Block("MINPAR")
    extpar = pyslha.Block("EXTPAR")

    # --- Fill the MINPAR block with the correct keys (indices)

    for key in values_dict:
        if key in MINPAR_KEYS:
            minpar[MINPAR_INDICES[key]] = values_dict[key]
        elif key in EXTPAR_KEYS:
            extpar[EXTPAR_INDICES[key]] = values_dict[key]

    # --- and add the block to the pyslha dictionary
    all_blocks["MINPAR"] = minpar
    all_blocks["EXTPAR"] = extpar

    # --- Add the generator input block if provided
    if  generator_input is not None:

        # --- Make sure generator input is a pyslha.Block
        if not isinstance(generator_input, pyslha.Block):
            raise TypeError("If generator_input is provided, it must "
                            "be an pyslha.Block.")

        all_blocks[generator_input] = generator_input


    # --- Pyslha Docs are filled with Blocks, Decays and Xsections.
    # --- For an SLHA input file, only the Blocks part is relevant.

    # --- Make the pyslha doc
    slha_doc = pyslha.Doc(all_blocks)

    # --- and write the SLHAfile
    pyslha.writeSLHAFile(slha_file_name, slha_doc)

def run_feynhiggs(fhpath, slha_file):
    """
    Run FeynHiggs on spectrum in slha_file
    """

    # --- Make sure the FeynHiggs path is valid and the SLHA file exists
    assert os.path.isdir(fhpath), "FeynHiggs path is not valid."
    assert os.path.isfile(slha_file), "SLHA file provided to FeynHiggs not found."

    process = subprocess.Popen(
        [os.path.join(fhpath, "x86_64-Linux/bin/FeynHiggs"), slha_file],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
        )

    process.wait()

    _, std_err_value = process.communicate()

    # --- FeynHiggs writes to std err in case of failure
    if std_err_value.strip() != "":
        print ("FeynHiggs failed:\n{0}\n\nProceeding to next point."
               .format(std_err_value))

        return None

    # --- This is added by FeynHiggs
    fh_corrected_slha_file = "{0}.fh-001".format(slha_file)

    return fh_corrected_slha_file

def find_lsp(massdict):
    """
    Returns a tuple with (pid, mass) of the LSP.
    Input: Mass dict of type pyslha.Block
    """

    # --- Raise exception if input is not a massdict
    if not isinstance(massdict, pyslha.Block):
        raise TypeError("Expected input of type pyslha.Block." "Got: ",
                        type(massdict))

    # --- Brute force way of getting only the supersymmetric particles from the
    # --- mass dict (these have PDG-codes consisting of seven characters)
    sparticles_dict = [ptuple for ptuple in massdict.items() if
                       len(str(abs(ptuple[0]))) == 7]

    # --- Return the entry with the smallest mass
    return min(sparticles_dict, key=lambda x: abs(x[1]))
