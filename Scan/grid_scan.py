# --- Inga Strumke

"""
Perform a grid scan over given ranges for m0, m12, A0, tanb and the soft Higgs
masses.
Local paths to SPheno and FeynHigg must be set.
Run with --test for test run.
"""


from argparse import ArgumentParser
import os
import shutil
import itertools
import pyslha

from scan_helper_functions import make_slha_file, run_feynhiggs, find_lsp
import SM_parameters
from spheno_functions import generator_block, generate_spectrum

# ================================================================================
# === CONSTANTS AND PATHS
# ================================================================================

from env_scan import SPHENO_PATH, FH_PATH, RESULTS_FILE, SCAN_SETTINGS

(KEEP_SLHA_INPUT, SLHA_INPUT_DIR,
 KEEP_SPHENO_OUTPUT, RESULT_DIR,
 KEEP_FEYNHIGGS_OUTPUT, FH_RESULT_DIR,
 HIGGS_MASS_CHECK, HIGGS_LIMITS) = SCAN_SETTINGS

# --- Instantiate results file
OPEN_RESULTS_FILE = open(RESULTS_FILE, 'w')
OPEN_RESULTS_FILE.write("m0 m12 A0 tanb m2H1 m2H2 m_higgs lsp m_lsp "
                        "m_stau_1 m_stau_2 m_nu_L m_neutralino_1\n")

# --- Get the Standard Model input values. These are in principle
# --- constants, but should be updated from PDG data
SMINPUTS = SM_parameters.main()

# --- Get the specific SPheno input block
SPHENOINPUT = generator_block()

# ================================================================================
# === Parse one argument, allowing a test run
# ================================================================================

PARSER = ArgumentParser(description='Plot scan result')
PARSER.add_argument('-t', '--testrun', action='store_true',
                    help="Test run with few points and no Higgs mass restriction.")
PARGS = PARSER.parse_args()

# ================================================================================
# === SCAN
# ================================================================================

# --- Set constants
SIGNMU = 1
MODSEL = 1

# --- Define scan ranges for all parameters

if PARGS.testrun:
    print "Running test scan."

    M0_RANGE = [i for i in range(0, 500, 100)]
    M12_RANGE = [i for i in range(0, 500, 100)]
    TANB_RANGE = [10]
    A0_RANGE = [i for i in range(-500, -400, 200)]
    M2H1_RANGE = [i for i in range(0, 1, 1)]
    M2H2_RANGE = [i for i in range(0, 1, 1)]

    HIGGS_LIMITS = (100, 150)

else:

    M0_RANGE = [i for i in range(0, 8200, 200)]
    M12_RANGE = [i for i in range(0, 8200, 200)]
    TANB_RANGE = [5, 10, 20, 50]
    A0_RANGE = [i for i in range(-8000, 8250, 250)]
    M2H1_RANGE = [i*1e5 for i in range(0, 60, 10)]
    M2H2_RANGE = [i*1e5 for i in range(0, 60, 10)]

    print ("Starting scan over ranges\n"
           "m0 = [{0}, {1}] GeV\n"
           "m12 = [{2}, {3}] GeV\n"
           "tan beta = {4}\n"
           "A0 = [{5}, {6}] GeV\n"
           "m2H1 = [{7}, {8}] GeV2\n"
           "m2H2 = [{9}, {10}] GeV2\n"
           .format(min(M0_RANGE), max(M0_RANGE), min(M12_RANGE),
                   max(M12_RANGE), TANB_RANGE, min(A0_RANGE),
                   max(A0_RANGE), min(M2H1_RANGE), max(M2H1_RANGE),
                   min(M2H2_RANGE), max(M2H2_RANGE))
           )

try:
    # --- Create all possible unique pairs from the scan ranges
    # --- (this replaces a nested for with one loop for each parameter)
    SCAN_RANGE = tuple(itertools.product(M0_RANGE, M12_RANGE, TANB_RANGE, A0_RANGE,
                                         M2H1_RANGE, M2H2_RANGE))
    N_POINTS = len(SCAN_RANGE)
    print "Considering {0} points.".format(N_POINTS)

    if N_POINTS > 10000:
        print "This may take a while..."

    # --- Scan over range
    N_ITER = 0
    for param_point in SCAN_RANGE:
        N_ITER += 1

        if N_ITER == N_POINTS/2:
            print "50% of points evaluated..."

        m0, m12, tanb, A0, m2H1, m2H2 = param_point

        # --- These are the values from the scan associated with names
        parameter_dict = {"m0" : m0, "tanb" : tanb, "m12" : m12,
                          "A0" : A0, "signmu" : SIGNMU,
                          "m2H1" : m2H1*1.0, "m2H2" : m2H2*1.0,
                         }

        slha_input_file = "scan_slha_input"

        make_slha_file(SMINPUTS,
                       parameter_dict,
                       slha_file_name=slha_input_file,
                       modsel_value=MODSEL,
                       generator_input=SPHENOINPUT)

        spheno_slha_file = "SPheno.spc"
        run_result = generate_spectrum(SPHENO_PATH,
                                       slha_input_file,
                                       slha_output_name=spheno_slha_file)

        # --- If SPheno returns False, continue to next point
        if not run_result:
            continue

        # --- Run Feynhiggs
        fh_slha_file = run_feynhiggs(FH_PATH, spheno_slha_file)
        if fh_slha_file is None:
            continue

        # --- Read result from slha file
        try:
            pyslha_doc = pyslha.readSLHAFile(fh_slha_file)
            mass_block = pyslha_doc.blocks['MASS']
            m_higgs = mass_block[25]

            lsp, m_lsp = find_lsp(mass_block)

            m_stau_1 = mass_block[1000015]
            m_stau_2 = mass_block[2000015]
            m_nu_L = mass_block[1000016]
            m_neutralino_1 = mass_block[1000022]

        # --- If there is a problem with the file itself (not found,
        # --- disc full, ...)
        except IOError:
            print ("Caught IOError. The scan will continue, but please check "
                  "if your disc is filling up.")
            continue

        # --- If the spectrum is not valid or pyslha cannot acces the file
        except (pyslha.AccessError, pyslha.ParseError):
            continue

        # --- Record results
        if HIGGS_MASS_CHECK:
            if not HIGGS_LIMITS[0] <= float(m_higgs) <= HIGGS_LIMITS[1]:
                continue

        OPEN_RESULTS_FILE.write("{0} {1} {2} {3} {4} {5} {6} {7} {8} "
                                "{9} {10} {11} {12}\n"
                        .format(m0, m12, A0, tanb, m2H1, m2H2, m_higgs, lsp, m_lsp,
                                m_stau_1, m_stau_2, m_nu_L, m_neutralino_1))

        # --- Keep or delete files produced
        if KEEP_SLHA_INPUT:
            unique_filename = ("slha_input_{0}_{1}_{2}_{3}_{4}_{5}"
                               .format(m0, m12, A0, tanb, m2H1, m2H2))
            shutil.move(slha_input_file, os.path.join(SLHA_INPUT_DIR, unique_filename))
        else:
            os.remove(slha_input_file)

        if KEEP_SPHENO_OUTPUT:
            unique_filename = ("spheno_output_{0}_{1}_{2}_{3}_{4}_{5}"
                               .format(m0, m12, A0, tanb, m2H1, m2H2))
            shutil.move(spheno_slha_file, os.path.join(RESULT_DIR, unique_filename))
        else:
            os.remove(spheno_slha_file)

        if KEEP_FEYNHIGGS_OUTPUT:
            unique_filename = ("feynhiggs_output_{0}_{1}_{2}_{3}_{4}_{5}"
                               .format(m0, m12, A0, tanb, m2H1, m2H2))
            shutil.move(fh_slha_file, os.path.join(FH_RESULT_DIR, unique_filename))
        else:
            os.remove(fh_slha_file)

    print "Done."

    # --- Clean up after SPheno
    if os.path.exists("Messages.out"):
        os.remove("Messages.out")
    OPEN_RESULTS_FILE.close()

# --- In case of KeyboardInterrupt, Write out how far the scan got
# --- and close results file
except (KeyboardInterrupt, SystemExit):
    try:
        print ("\n\nReached {0} points.\nClosing {1}.\nExiting."
               .format(N_ITER, RESULTS_FILE))
    except NameError:
        print ("\n\nScan did not start. If you're in a hurry, scan fewer points "
               "or increase the step size.")
