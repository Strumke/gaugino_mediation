# --- Inga Strumke

"""
In this file, the user must manually specify directories containing the
SPheno and FeynHiggs installations.
The user can specify which SLHA files to keep during the scan,
the output folder names and whether to keep only points within
a given Higgs mass range.
"""

import socket
import os

# === HERE IS WHERE YOU HAVE TO DO STUFF ======================================

# --- Set the path to the installations on your computer

SPHENO_PATH = None
              # Please replace None by the installation location of SPheno, e.g.
              # /home/myname/my_programs/SPheno-4.n.n

FH_PATH = None
          # Please replace None by the installation location of FeynHiggs, e.g.
          # /home/myname/my_programs/FeynHiggs-2.nn.n

# =============================================================================

# === IF YOU'RE USING SEVERAL MACHINES;
# === YOU CAN MAKE IT EASIER FOR YOURSELF BY USING HOSTNAME

#HOSTNAME = socket.gethostname()

#if HOSTNAME == "my_machine":
#    SPHENO_PATH = ""
#    FH_PATH = ""

#elif HOTNAME == "super_server":
#    SPHENO_PATH = ""
#    FH_PATH = ""

# =============================================================================

# === HERE IS WHERE YOU CAN CHANGE STUFF IF YOU WANT ==========================

# --- File for accepted points from the scan
RESULTS_FILE = "scan_results.dat"

# --- Option for keeping the SLHA input files created during the scan
KEEP_SLHA_INPUT = False

SLHA_INPUT_DIR = "scan_slha_input_files"
if KEEP_SLHA_INPUT and not os.path.isdir(SLHA_INPUT_DIR):
    os.makedirs(SLHA_INPUT_DIR)

# --- Decide whether to keep SPheno output SLHA files (for collider check)
KEEP_SPHENO_OUTPUT = True

RESULT_DIR = "slha_files_from_scan"
if KEEP_SPHENO_OUTPUT and not os.path.isdir(RESULT_DIR):
    os.makedirs(RESULT_DIR)

# --- Decide whether to keep FeynHiggs output SLHA files (for Higgs mass comparison)
KEEP_FEYNHIGGS_OUTPUT = False

FH_RESULT_DIR = "feynhiggs_slha_files_from_scan"
if KEEP_FEYNHIGGS_OUTPUT and not os.path.isdir(FH_RESULT_DIR):
    os.makedirs(FH_RESULT_DIR)

# --- If this is true, then only points with a Higgs mass in the
# --- range (123, 127) will be recorded.
HIGGS_MASS_CHECK = True
HIGGS_LIMITS = (123, 127)

# =============================================================================

SCAN_SETTINGS = (KEEP_SLHA_INPUT, SLHA_INPUT_DIR,
                 KEEP_SPHENO_OUTPUT, RESULT_DIR,
                 KEEP_FEYNHIGGS_OUTPUT, FH_RESULT_DIR,
                 HIGGS_MASS_CHECK, HIGGS_LIMITS)
