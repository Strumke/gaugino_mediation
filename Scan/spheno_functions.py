# --- Inga Strumke

"""
SPheno specific functions for the grid_scan.py module
"""

import os
import pyslha
import random
import subprocess

def generator_block():
    """
    Returns pyslha block containing the SPheno specific input
    """

    sphenoinput = pyslha.Block("SPhenoInput")
    sphenoinput[1] = -1              # error level
    sphenoinput[2] = 0               # SPA conventions
    sphenoinput[11] = 1              # calculate branching ratios
    sphenoinput[12] = 1.00000000E-04 # write only branching ratios larger than this value
    sphenoinput[21] = 0              # calculate cross section

    return sphenoinput

def generate_spectrum(sphenopath, slha_input_file, slha_output_name="SPheno.spc"):

    if len(slha_input_file) > 50:
        # -- In case of large filenames. Only a proble with SPheno.
        rename = True
        old_filename = slha_input_file
        slha_input_file = "_spheno_slha_in_{0}".format(random.randint(1, 1000))
    else:
        rename = False

    process = subprocess.Popen(
        [os.path.join(sphenopath, "bin/SPheno"), slha_input_file, slha_output_name],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
        )

    process.wait()

    std_out_value, std_err_value = process.communicate()

    # --- In case SPheno crashes,
    if std_err_value.strip() != "" or std_out_value.strip() != "":

        # --- it's probably just because the parameter point is numerically
        # --- difficult
        if ("There has been a problem during the run." in std_out_value
                or "STOP Subroutine TerminateProgram" in std_err_value):
            return False

        # --- but it can be for other reasons, in which case you'll want to
        # --- look into it.
        print "SPheno crashed with the following error"
        print std_err_value
        return False

    if rename:
        os.rename(slha_input_file, old_filename)

    return True

if __name__ == "__main__":

    from argparse import ArgumentParser

    from scan_helper_functions import make_slha_file
    from env_scan import SPHENO_PATH
    import SM_parameters

    PARSER = ArgumentParser(description='Plot scan result')
    PARSER.add_argument('-t', '--testrun', action='store_true',
                        help="Test run with few points and no Higgs mass restriction.")
    PARGS = PARSER.parse_args()

    if PARGS.testrun:

        SLHA_TEST = "test_input_file.slha"
        SPHENO_TEST_SPECTRUM = "test_spheno_spectrum.slha"

        print ("Performing test run.\nCreating the SLHA input file {0}"
               .format(SLHA_TEST))

        # --- Make one SLHA file with values of choice
        m0 = 700
        m12 = 250
        tanb = 10
        singnmu = 1
        A0 = -300
        m2H1 = 50*1e5
        m2H2 = 10*1e5

        SMINPUTS = SM_parameters.main()
        SPHENOINPUT = generator_block()

        # --- These are the values from the scan associated with names
        VALUES_DICT = {"m0" : m0,
                       "tanb" : tanb,
                       "m12" : m12,
                       "A0" : A0,
                       "signmu" : singnmu,
                       "m2H1" : m2H1,
                       "m2H2" : m2H2,
                       }


        make_slha_file(SMINPUTS, VALUES_DICT,
                       slha_file_name=SLHA_TEST, generator_input=SPHENOINPUT)

        print ("Writing resulting spectrum calculated by SPheno to {0}"
               .format(SPHENO_TEST_SPECTRUM))

        generate_spectrum(SPHENO_PATH, SLHA_TEST,
                          slha_output_name=SPHENO_TEST_SPECTRUM)
