#!/bin/bash
#./null_squarks [folder]
# --- Inga Strumke

# --- Runs the script on every file in the folder

folder=$1

# --- replace_mass [correct mass] [mass to change] [file to change]
# --- Replaces [mass to change] by the mass of [correct mass]
replace_mass()
{
  new_val=$(grep "^ .*# ~$1$" ${file} | sed -e "s/[ ]*[0-9]\+[ ]\+\([0-9.+E-]*\).*/\1/")
  sed -i -e "s/^\([ ]*[0-9]\+[ ]\+\)[0-9.+E-]\+\([ ]\+# ~$2\)$/\1${new_val}\2/" $3

}

for file in ${folder}/*; do

    # --- Check if file was already modified using this script
    if grep -q '# File modified by null_squarks' ${file}
    then
        echo "${file} was already processed by null_squarks. Not doing it again."
    else
        echo "Working with file ${file}"

        # --- Add a line to the top of the file stating that it has been modified
        sed -i '1s/^/# File modified by null_squarks.\n/' ${file}

        # --- Copy line containing m_b to the MASS block
        # --- i.e. line starts with (whitespaces) 5, and ends with # m_b(m_b), MSbar
        mbline=$(grep -m 1 '^\s*5.*#\s*m_b(m_b),\s*MSbar$' ${file})
        sed -i "/#   PDG code      mass          particle/a ${mbline}" ${file}

        # Change the value of Y_{u,c,d,s,e,mu} to be 0.00
        species=("u" "c" "d" "s" "e" "mu")
        for species in ${species[@]}; do
          sed -i -e "s/^\([ ]*[0-9][ ]\+[0-9][ ]\+\)[0-9.+E-]*\([ ]\+# Y_${species}(Q)\)/\10\.00000000E+00\2/" ${file}
          sed -i -e "s/^\([ ]*[0-9][ ]\+[0-9][ ]\+\)[0-9.+E-]*\([ ]\+# A_${species}(Q)\)/\10\.00000000E+00\2/" ${file}
        done

        # --- Set the value of M_({L,E,Q,U,D},22) to be its M_(&,11) version
        m_switches=("L" "E" "Q" "U" "D")
        for switch in ${m_switches[@]}; do
          new_val=$(grep "# M_(${switch},11)$" ${file} | sed -e "s/[ ]*[0-9]\+[ ]\+\([0-9.+E-]*\).*/\1/")
          sed -i -e "s/^\([ ]*[0-9]\+[ ]\+\)[0-9.+E-]*\([ ]\+# M_(${switch},22)\)/\1${new_val}\2/" ${file}
        done

        # --- Replace the values of the leptons so that {u,s,c}_{L,R} = d_{L,R},
        # --- mu_{L,R} = e_{L,R} and nu_muL = nu_eL
        quark_species=("u" "s" "c")
        handedness=("L" "R")
        for hand in ${handedness[@]}; do
          for qs in ${quark_species[@]}; do
            replace_mass "d_${hand}" "${qs}_${hand}" ${file}
          done
          replace_mass "e_${hand}-" "mu_${hand}-" ${file}
        done

        replace_mass "nu_eL" "nu_muL" ${file}
    fi
done
