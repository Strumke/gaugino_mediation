# --- Inga Strumke

"""
This program must be run prior to the collider_check.py, and serves to modify
the SPheno generated SLHA files so that MadGraph will accept them. This is done
by making the script null_squarks.sh executable, and then running it on all the
SLHA files.
"""

import os
import subprocess

from env_collider_2018 import LH_DIR

# --- SLHA files created by SPheno must be modified using a shell script

# --- First, make the script executable,
os.chmod("../null_squarks.sh", 0744)

# --- next, run it on the folder containing the SLHA files
process_fileop = subprocess.Popen(["../null_squarks.sh", LH_DIR],
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.PIPE
                                 )

_, std_err_value = process_fileop.communicate()
if std_err_value.strip() != "":
    print "Script null_squarks.sh failed. Please send me an email. I'm sorry."

print "Success!\nReady for collider check."
