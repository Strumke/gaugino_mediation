# --- Inga Strumke

"""
In this file, the user must manually specify directories containing the
CheckMATE installation.
Note that CheckMATE version 2 is required.
"""

import subprocess
import sys
import socket
import os

# === HERE IS WHERE YOU HAVE TO DO STUFF ======================================

# --- Set the path to the installation on your computer

CM_PATH = None
          # Please replace None by the installation location of CheckMATE, e.g.
          # /home/myname/my_programs/CheckMATE-2.n.nn

# =============================================================================

# === IF YOU'RE USING SEVERAL MACHINES;
# === YOU CAN MAKE IT EASIER FOR YOURSELF BY USING HOSTNAME

#HOSTNAME = socket.gethostname()

#if HOSTNAME == "my_machine":
#    CM_PATH = ""

#elif HOTNAME == "super_server":
#    CM_PATH = ""

# =============================================================================

# --- File to which the collider check allowed points will be written
CM_ALLOWED = "result_allowed_points.dat"

# =============================================================================

# === YOU ONLY NEED TO LOOK AT THIS IF YOU'VE CHANGED THINGS ==================

# -- Directory containing the SLHA files from the scan
LH_DIR = os.path.abspath("../../Scan/slha_files_from_scan")
try:
    FILES = os.listdir(LH_DIR)
except OSError:
    print ("Please run the grid scan first, and set the path to the SPheno "
           "SLHA files in env_collider.py before starting the collider check.")


# -- Directory containing the MadGraph cards
PROVIDED_CARDS = os.path.abspath("../PROVIDED_CARDS")
if not os.path.isdir(PROVIDED_CARDS):
    print ("Cannot find the directory PROVIDED_CARDS. Expected it to "
           "be one directory above this one. Please update the path or move it "
           "to the right location.\nExiting.")
    sys.exit()

# =============================================================================

# --- Get all ATLAS 8 TeV analyses for CheckMATE
analyses_file = os.path.join(CM_PATH,
                             "data/analysis_info/ATLAS_8TeV_analyses.txt")

proc1 = subprocess.Popen(["cat", analyses_file], stdout=subprocess.PIPE)

# --- Look for lines which do _not_ begin with a "#"
proc2 = subprocess.Popen(["grep", "-v", "#"], stdin=proc1.stdout,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)

# --- Cut out the first column (delimiter = " ")
proc3 = subprocess.Popen(["cut", "-d", " ", "-f1"], stdin=proc2.stdout,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)

proc1.stdout.close() # Allow proc1 to receive a SIGPIPE if proc2 exits.
proc2.stdout.close() # Allow proc2 to receive a SIGPIPE if proc3 exits.

std_out, error = proc3.communicate()

if error.strip() == "":
    # --- Don't include the last emtpy entry
    ANALYSIS_LIST = std_out.split("\n")[:-1]
    ANALYSES = ','.join(ANALYSIS_LIST)

else:
    print ("Failed to get the list of 8 TeV ATLAS analyses from the CheckMATE "
           "sub-directory /data/analysis/. Please make sure it exists.")
    ANALYSES = "atlas & 8TeV"
