# --- Inga Strumke

"""
Perform a collider check using CheckMATE (with MadGraph and Pythia) for the
slha files created by the grid scan.
Path to the CheckMATE installation must be set in env_collider_2018.py prior to run.
"""

import os
import shutil
import re
import subprocess

from env_collider_2018 import (CM_PATH, LH_DIR, FILES, ANALYSES,
                               PROVIDED_CARDS, CM_ALLOWED)

def all_subdirs_of(root_dir='.'):
    """
    List all sub-directories of the given directory (default is current
    directory).
    """

    result = []

    for subdir in os.listdir(root_dir):
        subdir_path = os.path.join(root_dir, subdir)
        if os.path.isdir(subdir_path):
            result.append(subdir_path)

    return result

def main(files):
    """
    For each file in the provided list of slha files,
    create a parameter file and run CheckMATE with it.
    CheckMATE does event generation, showering and hadronization.
    """

    # --- Open file for writing allowed points
    cm_allowed_open = open(CM_ALLOWED, 'w')
    cm_allowed_open.write("Filename Result r-value Analysis Signal_region\n")

    for _file in files:

        # --- Get LH file for run
        slha_file = os.path.join(LH_DIR, _file)

        # --- Make sure slha file exists
        assert os.path.isfile(slha_file), ("SLHA file not found inside {0}. Did "
                                           "you specify the correct path?"
                                           .format(LH_DIR))

        print "Starting on {0}".format(_file)

        # --- Copy the slha file to the Cards folder
        shutil.copy(slha_file, PROVIDED_CARDS)

        # --- Write info needed for CheckMATE, specific for the current file
        cm_file = "cm_run_parameters.dat"
        with open(cm_file, 'w') as open_cm_file:

            open_cm_file.write("## General Options\n"
                               "[Parameters]\n"
                               "OutputExists: overwrite\n"
                               "SkipParamCheck: True\n\n"
                               "Name: {0}\n"
                               "Analyses: {1}\n\n"
                               "## Process Information\n"
                               "[allew]\n"
                               "MGparam: {2}\n"
                               "MGprocess: {3}/proc_card_mg5.dat\n"
                               "MGrun: {3}/run_card.dat\n"
                               .format(_file, ANALYSES, slha_file, PROVIDED_CARDS))

        process_cm = subprocess.Popen([os.path.join(CM_PATH, "bin/CheckMATE"),
                                       cm_file],
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE,
                                     )
        std_out_value, std_err_value = process_cm.communicate()

        # --- Remove the slha file from the Cards folder
        os.remove(os.path.join(PROVIDED_CARDS, _file))

        if std_err_value.strip() != '':
            print "CheckMATE error occurred:"
            print std_err_value
            continue

        try:
            if "Allowed" in re.findall("Result:(.*)", std_out_value)[0]:
                result = "Allowed"
            else:
                result = "Excluded"

            r_value = float(re.findall("Result for r:(.*)", std_out_value)[0])

            analysis = re.findall("Analysis:(.*)", std_out_value)[0].strip()

            signal_region = re.findall("SR:(.*)", std_out_value)[0].strip()

        except IndexError:
            print "Could not find the required information in output:"
            continue

        print "Result: {0}".format(result)

        cm_allowed_open.write("{0} {1} {2} {3} {4}\n"
                              .format(_file.strip("cm_"), result, r_value,
                                      analysis, signal_region))

        print "Moving on."

if __name__ == "__main__":

    main(FILES)

    try:
        os.remove("cm_run_parameters.dat")
    except OSError:
        pass

    print ("\n~~~~~~~~~~~~~~~~~~~~~~~\n\n"
           "Finished all SLHA files.\n\n"
           "A summary of the allowed points is written to {0}.\n\n"
           "Detailed results are stored in\n{1}\n(and this folder is likely "
           "to be large, so you may want to remove it if you don't need it.)"
           .format(CM_ALLOWED, os.path.join(CM_PATH, "results")))
