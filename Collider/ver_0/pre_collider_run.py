# --- Inga Strumke

"""
This program must be run prior to the collider_check.py.
It modifies the SPheno generated SLHA files so that MadGraph will accept them.
This is done by making the script null_squarks.sh executable, and then running
it on all the SLHA files.
Next, MadGraph is initialized with the required settings and run cards.
"""

import os
import sys
import subprocess
import shutil

from env_collider_0 import LH_DIR, MG_PATH, MG_BROWSER

PROVIDED_CARDS = os.path.abspath("../PROVIDED_CARDS")
if not os.path.isdir(PROVIDED_CARDS):
    print ("Cannot find the directory PROVIDED_CARDS. Expected it to "
           "be one directory above this one. Please update the path or move it "
           "to the right location.\nExiting.")
    sys.exit()

# --- 1 ---
# --- SLHA files created by SPheno must be modified using a shell script

# --- First, make the script executable,
os.chmod("../null_squarks.sh", 0744)

# --- next, run it on the folder containing the SLHA files
process_fileop = subprocess.Popen(["../null_squarks.sh", LH_DIR],
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.PIPE
                                 )

_, std_err_value = process_fileop.communicate()
if std_err_value.strip() != "":
    print "Script null_squarks.sh failed. Please send an email. I'm sorry."

print "1/2 : SLHA files have been converted."

# --- 2 ---
# --- Prepare MadGraph for run (define particles, make run folder etc)
proc_card_addr = os.path.abspath(os.path.join(PROVIDED_CARDS,
                                              "proc_card_mg5.dat"))

process_mg = subprocess.Popen([os.path.join(MG_PATH, "bin/mg5_aMC"),
                               proc_card_addr],
                               cwd=MG_PATH,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE
                             )
_, std_err_value = process_mg.communicate()

if std_err_value.strip() != "":
    print ("MadGraph initialization failed. Please run manually in {0}:"
          .format(MG_PATH))
    print "$ ./bin/mg5_aMC"
    print "MG5_aMC> import model MSSM_SLHA2"
    print ("MG5_aMC> define ewsparticle = sve svm svt el- mul- ta1- er-"
          " mur- ta2- sve~ svm~ svt~ el+ mul+ ta1+ er+ mur+ ta2+"
          " n1 n2 n3 n4 x1+ x2+ x1- x2-")
    print "MG5_aMC> generate p p > ewsparticle ewsparticle"
    print "MG5_aMC> output PROC_mssm_all_ew"
    sys.exit()

print "2/2 : MadGraph has been initialized"

# --- 3 ---
# --- MadGraph writes the directory "PROC_mssm_all_ew".
# --- Copy all the files from PROVIDED_CARDS to PROC_mssm_all_ew/Cards
FILES = os.listdir(PROVIDED_CARDS)
for _file in FILES:
    full_file = os.path.join(PROVIDED_CARDS, _file)
    shutil.copy(full_file, os.path.join(MG_PATH, "PROC_mssm_all_ew/Cards"))

# --- 4 ---
# --- Decide whether to open a browser window during the MadGraph run
if not MG_BROWSER:
    MG_CONFIG_FILE = os.path.join(MG_PATH,
                                  "PROC_mssm_all_ew/Cards/me5_configuration.txt")

    # --- Remove any default setting
    subprocess.Popen(["sed", "-i", "/automatic_html_opening/d",
                      MG_CONFIG_FILE])

    # --- and set html opening to False
    subprocess.Popen(["sed", "-i", "$a automatic_html_opening=False",
                      MG_CONFIG_FILE])

    print "Ready to run MadGraph."
